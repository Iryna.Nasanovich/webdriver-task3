import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import page.GoogleCloudMainPage;
import page.GooglePricingCalculatorPage;

public class GoogleCalculatorTest {
    private WebDriver driver;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
    }

    @Test
    public void googleCloudService() {
        GooglePricingCalculatorPage googlePricingCalculatorPage = new GoogleCloudMainPage(driver)
                .openCloudService()
                .enableSearch()
                .chooseSearchResult();
        double actual = googlePricingCalculatorPage
                .switchToIframe()
                .setPricingCalculatorTab()
                .setNumberOfInstances()
                .setSeries()
                .setMachineType()
                .addGpu()
                .setGpuType()
                .setNumberOfGpu()
                .setLocalSsd()
                .setDatacenterLocation()
                .setCommitedUsage()
                .addToEstimate()
                .checkPriceIsCalculated()
                .selectEmailEstimate()
                .openNewTab()
                .openEmailGeneratingService()
                .generateRandomEmail()
                .copyGeneratedEmail()
                .enterGeneratedEmailToCalculator()
                .sendEmailWithTotalEstimate()
                .checkEmail()
                .getEmailedTotalEstimate();

        double expected = googlePricingCalculatorPage.calculatedTotalEstimate;

        Assert.assertTrue("Total estimates don't match", Double.compare(expected, actual) == 0);
    }

    @After
    public void tearDown() {
        driver.close();
        driver.quit();
    }

}