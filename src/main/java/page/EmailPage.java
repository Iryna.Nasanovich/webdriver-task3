package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import constant.GoogleCommonConstants;
import util.GoogleCloudPageUtils;

public class EmailPage extends GoogleCloudServicePage {

    private static String parent_tab;
    private static String child_tab;
    private static String generatedEmail;
    public double totalEstimateFromEmail;
    private static String emailedTotalEstimateLocator = "//h3";

    @FindBy(id = "necesary")
    private List<WebElement> cookiesNecessaryAgreeBtn;

    @FindBy(xpath = "//article[@id='cloud-site']//iframe")
    private WebElement frameLocator;

    @FindBy(xpath = "//a[@href='email-generator']")
    private WebElement emailGenerator;

    @FindBy(xpath = "//span[@class='genytxt']")
    private List<WebElement> emailLocalPart;

    @FindBy(id = "input_616")
    private WebElement emailInputField;

    @FindBy(xpath = "//md-dialog-actions//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']")
//"//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']")
    private WebElement sendEmailBtn;

    @FindBy(xpath = "//button[@onclick='egengo();']")
    private WebElement checkEmailBtn;

    @FindBy(id = "refresh")
    private WebElement refreshBtn;

    public EmailPage(WebDriver driver) {
        super(driver);
    }

    public EmailPage openNewTab() {
        driver.switchTo().newWindow(WindowType.TAB);
        Set<String> openedTabs = driver.getWindowHandles();
        if (openedTabs.size() == 2) {
            Iterator<String> itr = openedTabs.iterator();

            parent_tab = itr.next();
            child_tab = itr.next();
        }
        return this;
    }

    public EmailPage openEmailGeneratingService() {
        driver.get(GoogleCommonConstants.EMAIL_GENERATING_SERVICE_URL);
        return this;
    }

    public EmailPage generateRandomEmail() {
        GoogleCloudPageUtils.closeCookiesPopupWindow(cookiesNecessaryAgreeBtn);
        emailGenerator.click();
        return this;
    }

    public EmailPage copyGeneratedEmail() {

        generatedEmail = GoogleCloudPageUtils.buildEmail(emailLocalPart);
        return this;
    }

    public EmailPage enterGeneratedEmailToCalculator() {
        driver.switchTo().window(parent_tab);
        driver.switchTo().frame(frameLocator);
        driver.switchTo().frame("myFrame");
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, emailInputField);
        emailInputField.sendKeys(generatedEmail);
        return this;
    }

    public EmailPage sendEmailWithTotalEstimate() {
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, sendEmailBtn);
        sendEmailBtn.click();
        return this;
    }

    public EmailPage checkEmail() {
        driver.switchTo().window(child_tab);
        GoogleCloudPageUtils.closeCookiesPopupWindow(cookiesNecessaryAgreeBtn);
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, checkEmailBtn);
        checkEmailBtn.click();
        return this;
    }

    public double getEmailedTotalEstimate() {
        driver.switchTo().frame("ifmail");
        List<WebElement> totalEstimateFromEmailLocator = driver.findElements(By.xpath(emailedTotalEstimateLocator));
        if (totalEstimateFromEmailLocator.size() == 0) {
            driver.switchTo().defaultContent();
            refreshBtn.click();
            totalEstimateFromEmailLocator = driver.findElements(By.xpath(emailedTotalEstimateLocator));
        }
        totalEstimateFromEmail = GoogleCloudPageUtils
                .parseMessageToDouble(GoogleCloudPageUtils.buildString(totalEstimateFromEmailLocator));
        return totalEstimateFromEmail;
    }
}
