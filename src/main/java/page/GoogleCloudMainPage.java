package page;

import org.openqa.selenium.WebDriver;

import constant.GoogleCommonConstants;

public class GoogleCloudMainPage extends GoogleCloudServicePage {

    public GoogleCloudMainPage(WebDriver driver) {
        super(driver);
    }

    public GoogleSearchResultsPage openCloudService() {
        driver.get(GoogleCommonConstants.GOOGLE_CLOUD_SERVICE_URL);
        return new GoogleSearchResultsPage(driver);
    }
}
