package page;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import constant.GoogleCommonConstants;
import util.GoogleCloudPageUtils;

public class GoogleSearchResultsPage extends GoogleCloudServicePage {

    @FindBy(xpath = "//*[@class='mb2a7b']")
    private WebElement searchIcon;

    @FindBy(xpath = "//input[@class='mb2a7b']")
    private WebElement searchInputField;

    @FindBy(xpath = "//a[@class=\'gs-title\'and@href]")
    private List<WebElement> searchResults;

    public GoogleSearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public GoogleSearchResultsPage enableSearch() {
        searchIcon.click();
        searchInputField.sendKeys(GoogleCommonConstants.SEARCH_REQUEST, Keys.ENTER);
        return this;
    }

    public GooglePricingCalculatorPage chooseSearchResult() {
        GoogleCloudPageUtils.clickOnListValue(searchResults, GoogleCommonConstants.SEARCH_REQUEST);
        return new GooglePricingCalculatorPage(driver);
    }


}
