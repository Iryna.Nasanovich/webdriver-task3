package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import constant.GoogleCommonConstants;
import util.GoogleCloudPageUtils;

public class GooglePricingCalculatorPage extends GoogleCloudServicePage {

    private static final String tabsLocator = "//md-pagination-wrapper//span[@class='ng-binding']";
    private static final String seriesContainerLocator = "//div[@id='select_container_125']//div[@class='md-text ng-binding']";//
    private static final String machineTypeContainerLocator = "//div[@id='select_container_127']//div[@class='md-text'or@class='md-text ng-binding']";
    private static final String gpuTypeContainerLocator = "//div[@id='select_container_507']//div[@class='md-text ng-binding']";
    private static final String numberOfGpuContainerLocator = "//div[@id='select_container_509']//div[@class='md-text ng-binding']";
    private static final String localSsdContainerLocator = "//div[@id='select_container_466']//div[@class='md-text ng-binding']";
    private static final String datacenterLocationContainerLocator = "//div[@id='select_container_133']//div[@class='md-text ng-binding']";
    private static final String commitedUsageContainerLocator = "//div[@id='select_container_140']//div[@class='md-text']";
    private static final String checkboxLocator = "//div[@class='md-label']";
    public double calculatedTotalEstimate;

    @FindBy(xpath = "//button[@class='devsite-snackbar-action']")
    private List<WebElement> cookiesOkBtn;

    @FindBy(xpath = "//article[@id='cloud-site']//iframe")
    private WebElement frameLocator;

    @FindBy(id = "input_99")
    private WebElement computerEngineElement;

    @FindBy(xpath = "//md-select[@id='select_124']")
    private WebElement seriesDropdownLocator;

    @FindBy(id = "select_126")
    private WebElement machineTypeDropdownLocator;

    @FindBy(id = "select_506")
    private WebElement gpuTypeDropdownLocator;

    @FindBy(id = "select_508")
    private WebElement numberOfGpuDropdownLocator;

    @FindBy(xpath = "//md-select[@id='select_465']")
    private WebElement localSsdDropdownLocator;

    @FindBy(xpath = "//md-select[@id='select_132']")
    private WebElement datacenterLocationDropdownLocator;

    @FindBy(xpath = "//md-select[@id='select_139']")
    private WebElement commitedUsageDropdownLocator;

    @FindBy(xpath = "//button[@class='md-raised md-primary cpc-button md-button md-ink-ripple']")
    private WebElement addToEstimateBtn;

    @FindBy(xpath = "//b[@class='ng-binding']")
    private WebElement calculatedPrice;

    @FindBy(id = "Email Estimate")
    private WebElement sendEmailEstimateBtn;

    public GooglePricingCalculatorPage(WebDriver driver) {
        super(driver);
    }

    public GooglePricingCalculatorPage switchToIframe() {
        GoogleCloudPageUtils.closeCookiesPopupWindow(cookiesOkBtn);
        driver.switchTo().frame(frameLocator);
        driver.switchTo().frame("myFrame");
        return this;
    }

    public GooglePricingCalculatorPage setPricingCalculatorTab() {
        List<WebElement> tabsValues = driver.findElements(By.xpath(tabsLocator));
        GoogleCloudPageUtils.clickOnListValue(tabsValues, GoogleCommonConstants.CALCULATOR_TAB);
        return this;
    }

    public GooglePricingCalculatorPage setNumberOfInstances() {
        computerEngineElement.click();
        computerEngineElement.sendKeys(GoogleCommonConstants.NUMBER_OF_INSTANCES);
        return this;
    }

    public GooglePricingCalculatorPage setSeries() {
        seriesDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, seriesContainerLocator);
        List<WebElement> seriesDropdownValue = driver.findElements(By.xpath(seriesContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(seriesDropdownValue, GoogleCommonConstants.SERIES);
        return this;
    }

    public GooglePricingCalculatorPage setMachineType() {
        machineTypeDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, machineTypeContainerLocator);
        List<WebElement> machineTypeDropdownValue = driver.findElements(By.xpath(machineTypeContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(machineTypeDropdownValue, GoogleCommonConstants.MACHINE_TYPE);
        return this;
    }

    public GooglePricingCalculatorPage addGpu() {
        List<WebElement> addGpuCheckbox = driver.findElements(By.xpath(checkboxLocator));
        GoogleCloudPageUtils.clickOnListValue(addGpuCheckbox, GoogleCommonConstants.GPU);
        return this;
    }

    public GooglePricingCalculatorPage setGpuType() {
        gpuTypeDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, gpuTypeContainerLocator);
        List<WebElement> gpuTypeDropdownValue = driver.findElements(By.xpath(gpuTypeContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(gpuTypeDropdownValue, GoogleCommonConstants.GPU_TYPE);
        return this;
    }

    public GooglePricingCalculatorPage setNumberOfGpu() {
        numberOfGpuDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, numberOfGpuContainerLocator);
        List<WebElement> numberOfGpuDropdownValue = driver.findElements(By.xpath(numberOfGpuContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(numberOfGpuDropdownValue, GoogleCommonConstants.NUMBER_OF_GPUS);
        return this;
    }

    public GooglePricingCalculatorPage setLocalSsd() {
        localSsdDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, localSsdContainerLocator);
        List<WebElement> localSsdDropdownValue = driver.findElements(By.xpath(localSsdContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(localSsdDropdownValue, GoogleCommonConstants.LOCAL_SSD);
        return this;
    }

    public GooglePricingCalculatorPage setDatacenterLocation() {
        datacenterLocationDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, datacenterLocationContainerLocator);
        List<WebElement> datacenterLocationDropdownValue = driver.findElements(By.xpath(datacenterLocationContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(datacenterLocationDropdownValue, GoogleCommonConstants.DATACENTER_LOCATION);
        return this;
    }

    public GooglePricingCalculatorPage setCommitedUsage() {
        commitedUsageDropdownLocator.click();
        GoogleCloudPageUtils.waitForElementToBeClickable(driver, commitedUsageContainerLocator);
        List<WebElement> commitedUsageDropdownValue = driver.findElements(By.xpath(commitedUsageContainerLocator));
        GoogleCloudPageUtils.clickOnListValue(commitedUsageDropdownValue, GoogleCommonConstants.COMMITED_USAGE);
        return this;
    }

    public GooglePricingCalculatorPage addToEstimate() {
        addToEstimateBtn.click();
        return this;
    }

    public GooglePricingCalculatorPage checkPriceIsCalculated() {
        calculatedTotalEstimate = GoogleCloudPageUtils.parseMessageToDouble(calculatedPrice.getText());
        return this;
    }

    public EmailPage selectEmailEstimate() {
        sendEmailEstimateBtn.click();
        return new EmailPage(driver);
    }
}
